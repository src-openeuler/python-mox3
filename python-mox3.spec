%global _empty_manifest_terminate_build 0
Name:		python-mox3
Version:	1.1.0
Release:	1
Summary:	Mock object framework for Python
License:	Apache-2.0
URL:		https://docs.openstack.org/mox3/latest/
Source0:	https://files.pythonhosted.org/packages/90/ac/2220e111a63e32872567d6e677b4b36163c0c5ac766ef76d00dee3eabdf9/mox3-1.1.0.tar.gz
BuildArch:	noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-fixtures

Requires:	python3-pbr
Requires:	python3-fixtures

%description
Mox3 is an unofficial port of the Google mox framework 
(http://code.google.com/p/pymox/) to Python 3. 
It was meant to be as compatible with mox as possible, 
but small enhancements have been made. The library was 
tested on Python version 3.2, 2.7 and 2.6.

%package -n python3-mox3
Summary:	Mock object framework for Python
Provides:	python-mox3
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%description -n python3-mox3
Mox3 for python3.

%package help
Summary:	Development documents and examples for mox3
Provides:	python3-mox3-doc

%description help
Development documents and examples for mox3.

%prep
%autosetup -n mox3-1.1.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-mox3 -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Jul 13 2022 Xu Jin <jinxu@kylinos.cn> - 1.1.0-1
- Package Spec generated
